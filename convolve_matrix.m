function new_img = convolve_matrix(img, kernel)
% make sure data type of image and kernel are the same
% convert image to image double (Do not use double() instead of im2Double() )
img = im2double(img);
% convert kernel as well
kernel = im2double(kernel);

% get kernel size
[krows, kcols] = size(kernel);
% error checking kernel dimension
if (mod(krows,2) == 0 || mod(kcols,2) == 0)
    error("Kernel sizes must have odd dimension. Kernel used: " + krows + "x" + kcols)
    return;
end

% get image size
[irows, icols] = size(img);
% calculate padding
arows = floor(krows/2);
acols = floor(kcols/2);

%pad the image
img = padarray(img,[arows acols], 0, 'both');
%create a new image to store data in
new_img = zeros(irows,icols);
% loop from outside row border
for i = arows+1:irows+arows
    % do same for cols
    for j = acols+1:icols+acols 
        % set the temporary matrix equal to the kernel size
        tempElem = img(i-arows:i+arows, j-acols:j+acols); 
        % multiply temporary matrix by kernel
        tempElem = tempElem .* kernel;
        % create new pixel from sum of temporary matrix
        new_img(i-arows,j-acols) = sum(tempElem(:));
    end
end

% strip out any uncessary padding
new_img = new_img(1:irows,1:icols);

end