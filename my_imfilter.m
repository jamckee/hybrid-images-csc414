%Check if we have RGB or grayscale
function new_img = my_imfilter(img, kernel)

% check number of channels
if(size(img,3)==3)
%if RGB do:
   % separate image into 3 channels
   r_channel = img(:,:,1);
   g_channel = img(:,:,2);
   b_channel = img(:,:,3);
   % process channels independently
   r_channel = convolve_matrix(r_channel, kernel);
   g_channel = convolve_matrix(g_channel, kernel);
   b_channel = convolve_matrix(b_channel, kernel);
   % recombine channels into new image
   new_img = cat(3, r_channel, g_channel, b_channel);
%if grayscale do:
else
   % process single channel as gray matrix
   new_img = convolve_matrix(img, kernel);
end
end