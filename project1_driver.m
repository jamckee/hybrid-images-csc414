% temp_img = imread('beaver.jpg');

% preTask(temp_img);
% image arrays
img1 = {imread('data/bird.bmp'),imread('data/bicycle.bmp'),imread('data/cat.bmp'),imread('data/einstein.bmp'),imread('data/fish.bmp')};
img2 = {imread('data/plane.bmp'),imread('data/motorcycle.bmp'),imread('data/dog.bmp'),imread('data/marilyn.bmp'),imread('data/submarine.bmp')};
% file output names
img3 = {'birdplane','motorbike','catdog','marilstein','fishsub'};

% create overview graphic for frequency domain hybrid image
% taskOneFullDisplay(img1{4}, img2{4}, 17);
% loop through all images to make images
for x=1:5
    taskOneHybrid(img1{x}, img2{x}, img3{x}, 17);
    taskTwoHybrid(img1{x}, img2{x}, img3{x}, 2);
end


% Spatial Domain hybrid image
%%
function taskTwoHybrid(temp_img1, temp_img2, name, sigma)
% create hybrid image for 5 sizes decimating the image by 1/2 each pass
hybrid1 = gen_hybrid_image(temp_img1, temp_img2, sigma, 'spatial');
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid2 = gen_hybrid_image(temp_img1, temp_img2, sigma, 'spatial');
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid3 = gen_hybrid_image(temp_img1, temp_img2, sigma, 'spatial');
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid4 = gen_hybrid_image(temp_img1, temp_img2, sigma, 'spatial');
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid5 = gen_hybrid_image(temp_img1, temp_img2, sigma, 'spatial');

% Generate pyramid image
[y1, x1] = size(hybrid1);
[y2, x2] = size(hybrid2);
[y3, x3] = size(hybrid3);
[y4, x4] = size(hybrid4);
[y5, x5] = size(hybrid5);
% check if we have RGB or grayscale image
if (size(hybrid1,3) == 3)
    x1 = x1/3;x2 = x2/3;x3 = x3/3;x4 = x4/3;x5 = x5/3;
end

%combine individual hybrid images into 1 pyramid image
combined_img = zeros(y1, x1+x2+x3+x4+x5, 3);
combined_img(1:y1, 1:x1, :) = hybrid1(1:y1, 1:x1, :);
combined_img(y1-y2+1:y1, x1:x1+x2-1, :) = hybrid2(1:y2, 1:x2, :);
combined_img(y1-y3+1:y1, x1+x2:x1+x2+x3-1, :) = hybrid3(1:y3, 1:x3, :);
combined_img(y1-y4+1:y1, x1+x2+x3:x1+x2+x3+x4-1, :) = hybrid4(1:y4, 1:x4, :);
combined_img(y1-y5+1:y1, x1+x2+x3+x4:x1+x2+x3+x4+x5-1, :) = hybrid5(1:y5, 1:x5, :);

% write each hybrid image out to its own file
imwrite(hybrid1, strcat('out/', name, '.conv.1.jpg'));
imwrite(hybrid2, strcat('out/', name, '.conv.2.jpg'));
imwrite(hybrid3, strcat('out/', name, '.conv.3.jpg'));
imwrite(hybrid4, strcat('out/', name, '.conv.4.jpg'));
imwrite(hybrid5, strcat('out/', name, '.conv.5.jpg'));
imwrite(combined_img, strcat('out/',name,'.conv.combined.jpg'));
end

% Frequency Domain hybrid images
function taskOneHybrid(temp_img1, temp_img2, name, sigma)
% create hybrid image for 5 sizes decimating the image by 1/2 each pass
hybrid1 = gen_hybrid_image(temp_img1, temp_img2, sigma);
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid2 = gen_hybrid_image(temp_img1, temp_img2, sigma);
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid3 = gen_hybrid_image(temp_img1, temp_img2, sigma);
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid4 = gen_hybrid_image(temp_img1, temp_img2, sigma);
temp_img1 = decimate(temp_img1); temp_img2 = decimate(temp_img2);
hybrid5 = gen_hybrid_image(temp_img1, temp_img2, sigma);

%get sizes for creating a pyramid of images
[y1, x1] = size(hybrid1);
[y2, x2] = size(hybrid2);
[y3, x3] = size(hybrid3);
[y4, x4] = size(hybrid4);
[y5, x5] = size(hybrid5);
% check if we have a RGB or grayscale image
if (size(hybrid1,3) == 3)
    x1 = x1/3;x2 = x2/3;x3 = x3/3;x4 = x4/3;x5 = x5/3;
end
% combine individual images into single pyarmid
combined_img = zeros(y1, x1+x2+x3+x4+x5, 3);
combined_img(1:y1, 1:x1, :) = hybrid1(1:y1, 1:x1, :);
combined_img(y1-y2+1:y1, x1:x1+x2-1, :) = hybrid2(1:y2, 1:x2, :);
combined_img(y1-y3+1:y1, x1+x2:x1+x2+x3-1, :) = hybrid3(1:y3, 1:x3, :);
combined_img(y1-y4+1:y1, x1+x2+x3:x1+x2+x3+x4-1, :) = hybrid4(1:y4, 1:x4, :);
combined_img(y1-y5+1:y1, x1+x2+x3+x4:x1+x2+x3+x4+x5-1, :) = hybrid5(1:y5, 1:x5, :);

% write each image to output file
imwrite(hybrid1, strcat('out/', name, '.fd.1.jpg'));
imwrite(hybrid2, strcat('out/', name, '.fd.2.jpg'));
imwrite(hybrid3, strcat('out/', name, '.fd.3.jpg'));
imwrite(hybrid4, strcat('out/', name, '.fd.4.jpg'));
imwrite(hybrid5, strcat('out/', name, '.fd.5.jpg'));
imwrite(combined_img, strcat('out/',name,'.fd.combined.jpg'));
end
%% 
% 

% using frequency domain hybrid image create a full information display
function taskOneFullDisplay(temp_img, temp_img2, sigma)
    % prepare images for processing
    temp_img = rgb2gray(temp_img);
    temp_img = double(temp_img);
    temp_img2 = rgb2gray(temp_img2);
    temp_img2 = double(temp_img2);
    %get image size
    [y, x] = size(temp_img);
    
    %create filters
    hp_filter = fspecial('gaussian', [y x], sigma);
    hp_filter = hp_filter ./ max(hp_filter(:));
    hp_filter = ones(size(hp_filter)) - hp_filter;
    lp_filter = fspecial('gaussian', [y x], sigma);
    lp_filter = lp_filter ./ max(lp_filter(:));
    
    % image 1 processing
    F = fft2(temp_img);
    s = fftshift(F);
    % create filtered images
    lFiltered = s .* lp_filter;
    hFiltered = s .* hp_filter;
    % return images back to a displayable format
    ffiL = real(ifft2(ifftshift(lFiltered)));
    ffiH = real(ifft2(ifftshift(hFiltered)));
    
    % image 2 processing
    F2 = fft2(temp_img2);
    s2 = fftshift(F2);
    % filter data
    lFiltered2 = s2 .* lp_filter;
    hFiltered2 = s2 .* hp_filter;
    % return images back to a displayable format
    ffiL2 = real(ifft2(ifftshift(lFiltered2)));
    ffiH2 = real(ifft2(ifftshift(hFiltered2)));
    
    % create hybrid image
    hybrid = ffiH + ffiL2;
    
    % Create large 3x5 figure chart
    title('Task One');
    subplot(3,5,1);
    imshow(temp_img,[]);
    title('Original Image 1');
    subplot(3,5,3);
    imshow(hybrid, []);
    title('Hybrid Image');
    subplot(3,5,5);
    imshow(temp_img2,[]);
    title('Original Image 2');
    subplot(3,5,6);
    imagesc(20*log(abs(s)));
    title('Image 1 Magnitude');
    subplot(3,5,7);
    imagesc(20*log(abs(lFiltered)));
    title('Image 1 Low Mag');    
    subplot(3,5,8);
    imshow(ffiL,[]);
    title('Image 1 Low Pass');
    subplot(3,5,9);
    imagesc(20*log(abs(hFiltered)));
    title('Image 1 High Mag');    
    subplot(3,5,10);
    imshow(ffiH,[]);
    title('Image 1 High Pass');
    subplot(3,5,11);
    imagesc(20*log(abs(s2)));
    title('Image 2 Magnitude');
    subplot(3,5,12);
    imagesc(20*log(abs(lFiltered2)));
    title('Image 2 Low Mag');    
    subplot(3,5,13);
    imshow(ffiL2,[]);
    title('Image 2 Low Pass');
    subplot(3,5,14);
    imagesc(20*log(abs(hFiltered2)));
    title('Image 2 High Mag');    
    subplot(3,5,15);
    imshow(ffiH2,[]);
    title('Image 2 High Pass');
    
end

%function to test convolution function against built-in imfilter function
function preTask(temp_img)
colonel = fspecial('gaussian',7,1);

%Create Images to use for comparison
t1 = imfilter(temp_img, colonel);
t1 = decimate(t1);
t2 = imfilter(t1, colonel);
t2 = decimate(t2);
t3 = imfilter(t2, colonel);
t3 = decimate(t3);
t4 = imfilter(t3, colonel);
t4 = decimate(t4);
t5 = imfilter(t4, colonel);
t5 = decimate(t5);
plot_five(t1, t2, t3, t4, t5, 'Built-in Beaver Plot')

%bad kernel testing
%colonel = zeros([4,4]);
% check color version
u1 = my_imfilter(temp_img, colonel);
u1 = decimate(u1);
u2 = my_imfilter(u1, colonel);
u2 = decimate(u2);
u3 = my_imfilter(u2, colonel);
u3 = decimate(u3);
u4 = my_imfilter(u3, colonel);
u4 = decimate(u4);
u5 = my_imfilter(u4, colonel);
u5 = decimate(u5);
plot_five(u1, u2, u3, u4, u5, 'Custom Color Beaver Plot')

%check grayscale version
u1 = my_imfilter(rgb2gray(temp_img), colonel);
u1 = decimate(u1);
u2 = my_imfilter(u1, colonel);
u2 = decimate(u2);
u3 = my_imfilter(u2, colonel);
u3 = decimate(u3);
u4 = my_imfilter(u3, colonel);
u4 = decimate(u4);
u5 = my_imfilter(u4, colonel);
u5 = decimate(u5);
plot_five(u1,u2,u3,u4,u5, 'Custom Gray Scale');
end

%decimates image by 1/2
function [decimated] = decimate(orig_img)
    % prepare image
    orig_img = im2double(orig_img);
    % check number of channels
    if (size(orig_img,3) == 3)
        % split rgb into separate channels
        r_chan = orig_img(:,:,1);
        g_chan = orig_img(:,:,2);
        b_chan = orig_img(:,:,3);
        % decimate each channel
        r_chan = r_chan(1:2:end, 1:2:end);
        g_chan = g_chan(1:2:end, 1:2:end);
        b_chan = b_chan(1:2:end, 1:2:end);
        % combine images back into 1
        decimated = cat(3, r_chan, g_chan, b_chan);
    else
        % decimate single gray channel
        decimated = orig_img(1:2:end, 1:2:end);
    end
end

%plot 5 images in a row for comparison
function plot_five(i1, i2, i3, i4, i5, fig_name)
figure('Name',fig_name)
title(fig_name)
subplot(1,5,1)
imshow(i1,[])
title('Plot 1')

subplot(1,5,2)
imshow(i2,[])
title('Plot 2')

subplot(1,5,3)
imshow(i3,[])
title('Plot 3')

subplot(1,5,4)
imshow(i4,[])
title('Plot 4')

subplot(1,5,5)
imshow(i5,[])
title('Plot 5')
end
%% 
%