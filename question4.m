img = imread('data\BookShelf.jpg');
timechart = zeros(12, 31);
max_iteration = 16;
parfor i = 1:max_iteration
    timg = imresize(img, i/max_iteration);
    for x = 3:15
        h = fspecial('gaussian',x, 4);
        h = h ./ max(h(:));
        tic;
        imfilter(timg, h);
        time = toc;
        timechart(i, x-2) = time;
    end

end

plotmatrix(timechart);